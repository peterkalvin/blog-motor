package userrepository

import (
	sqlite "blog-motor/Repositories/SQLite"
	"errors"
)

// GetAllUsers - Get all user IDs
// @return {[]]float64} - User IDs
// @return {error}
func GetAllUsers() ([]float64, error) {
	db := sqlite.DB
	rows, err := db.Query("SELECT id FROM users ")
	if err == nil {
		var ids []float64
		for rows.Next() {
			var id float64
			err = rows.Scan(&id)
			if err == nil {
				ids = append(ids, id)
			} else {
				// If scan can't run
				rows.Close()
				return nil, err
			}
		}
		return ids, nil
	}
	// If the query can't run
	return nil, err
}

// GetUserByEmail - Get the User ID by email
// @param {string} - Email address
// @return {float64} - User ID
// @return {error}
func GetUserByEmail(email string) (float64, error) {
	db := sqlite.DB
	rows, err := db.Query("SELECT id FROM users WHERE email = ?", email)
	if err == nil {
		for rows.Next() {
			var id float64
			err = rows.Scan(&id)
			if err == nil {
				rows.Close()
				return id, nil
			}
			// If scan can't run
			rows.Close()
			return 0, err
		}
	}
	// If the query can't run
	return 0, errors.New("User not found")
}

// GetHashByEmail - Get the user's password hash by email
// @param {string} - Email address
// @return {string} - Password hash string
// @return {error}
func GetHashByEmail(email string) (string, error) {
	db := sqlite.DB
	rows, err := db.Query("SELECT password FROM users WHERE email = ?", email)
	if err == nil {
		for rows.Next() {
			var password []byte
			err = rows.Scan(&password)
			if err == nil {
				rows.Close()
				return string(password), nil
			}
			// If scan can't run
			rows.Close()
			return "", err
		}
	}
	// If the query can't run
	return "", err
}
