package userrepository

import (
	sqlite "blog-motor/Repositories/SQLite"
	"testing"

	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/crypto/bcrypt"
)

// TestGetUserByEmail - Testing the GetUserByEmail function
func TestGetUserByEmail(t *testing.T) {
	// Setup database connection
	sqlite.ConnectSQLite("../../")
	db := sqlite.DB

	// Create password hash
	hash, _ := bcrypt.GenerateFromPassword([]byte("1234"), 10)
	// Upload test user
	email := "teszt@teszt.hu"
	_, err := db.Exec("INSERT INTO users (email, password) VALUES ('" + email + "', '" + string(hash) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}

	// Sample data struct
	var (
		SampleData = []struct {
			email string
			err   string
		}{
			{
				email: email,
			},
			{
				email: "teszt@teszt.com",
				err:   "User not found",
			},
		}
	)

	// Test function
	for _, sd := range SampleData {
		_, err := GetUserByEmail(sd.email)
		if err != nil && err.Error() != sd.err {
			t.Errorf(`"%s" => unexpected error: "%v"`, sd.email, err)
		}
	}

	// Delete test user
	rows, err := db.Prepare("DELETE FROM users WHERE email = ?")
	if err == nil {
		exec, err := rows.Exec(email)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
}

// TestGetHashByEmail - Testing the GetHashByEmail function
func TestGetHashByEmail(t *testing.T) {
	// Setup database connection
	sqlite.ConnectSQLite("../../")
	db := sqlite.DB

	// Create password hash
	hash, _ := bcrypt.GenerateFromPassword([]byte("1234"), 10)
	// Upload test user
	email := "teszt@teszt.hu"
	_, err := db.Exec("INSERT INTO users (email, password) VALUES ('" + email + "', '" + string(hash) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}

	// Test function
	hashStr, err := GetHashByEmail(email)
	if err != nil {
		t.Errorf(`"%s" => unexpected error: "%v"`, email, err)
	} else if bcrypt.CompareHashAndPassword([]byte(hashStr), []byte("1234")) != nil {
		t.Errorf(`"%s" => unexpected error: "%v"`, email, "Passwords not match")
	}

	// Delete test user
	rows, err := db.Prepare("DELETE FROM users WHERE email = ?")
	if err == nil {
		exec, err := rows.Exec(email)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
}
