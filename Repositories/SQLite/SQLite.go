package sqlite

import (
	"database/sql"
)

// DB - The database pointer
var DB *sql.DB

// ConnectSQLite - Sets up database connection
// @param {string} - Database file directory
func ConnectSQLite(path string) {
	// Open database file
	DB, _ = sql.Open("sqlite3", path+"database.db")
}
