package commentrepository

import (
	sqlite "blog-motor/Repositories/SQLite"
	"fmt"
	"strconv"
	"time"
)

// AddComment - Insert new comment
// @param {int} - Post ID
// @param {float64} - User ID
// @param {string} - Comment body
// @return {error}
func AddComment(postID int, userID float64, body string) error {
	// Get current date and time in unix
	currentDateTime := time.Now().Unix()
	db := sqlite.DB
	_, err := db.Exec("INSERT INTO comments (post, user, body, datetime) VALUES ('" + strconv.Itoa(postID) + "', '" + fmt.Sprintf("%f", userID) + "', '" + body + "', '" + strconv.FormatInt(currentDateTime, 10) + "')")
	if err == nil {
		return nil
	}
	// If the query can't run
	return err
}

// GetCommentsNumByUser - Get comments number
// @param {float64} - User ID
// @param {int} - Days
// @return {int} - Comments number
func GetCommentsNumByUser(userID float64, days int) int {
	// Get current date and time in unix
	currentDateTime := time.Now().Unix()
	db := sqlite.DB
	rows, err := db.Query("SELECT datetime FROM comments WHERE user = ?", fmt.Sprintf("%f", userID))
	if err == nil {
		var comments []int
		for rows.Next() {
			var datetime int
			err = rows.Scan(&datetime)
			if err == nil {
				// Check date and time
				if datetime > int(currentDateTime)-days*84000 {
					comments = append(comments, datetime)
				}
			} else {
				// If scan can't run
				rows.Close()
				return 0
			}
		}
		rows.Close()
		return len(comments)
	}
	// If the query can't run
	return 0
}
