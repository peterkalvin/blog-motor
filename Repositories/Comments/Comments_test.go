package commentrepository

import (
	sqlite "blog-motor/Repositories/SQLite"
	"fmt"
	"strconv"
	"testing"
	"time"

	postrepository "blog-motor/Repositories/Posts"
	userrepository "blog-motor/Repositories/Users"

	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/crypto/bcrypt"
)

// TestGetCommentsNumByUser - Testing the GetCommentsNumByUser function
func TestGetCommentsNumByUser(t *testing.T) {
	// Setup database connection
	sqlite.ConnectSQLite("../../")
	db := sqlite.DB

	// Create password hash
	hash, _ := bcrypt.GenerateFromPassword([]byte("1234"), 10)
	// Upload test user
	email := "teszt@teszt.hu"
	_, err := db.Exec("INSERT INTO users (email, password) VALUES ('" + email + "', '" + string(hash) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	userID, _ := userrepository.GetUserByEmail(email)

	// Upload test post
	currentDateTime := time.Now().Unix()
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID) + "', '', '', '" + strconv.FormatInt(currentDateTime, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	postID, _ := postrepository.GetLastByUser(userID)

	// Upload test comments
	_, err = db.Exec("INSERT INTO comments (post, user, body, datetime) VALUES ('" + strconv.Itoa(postID) + "', '" + fmt.Sprintf("%f", userID) + "', '', '" + strconv.FormatInt(currentDateTime, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO comments (post, user, body, datetime) VALUES ('" + strconv.Itoa(postID) + "', '" + fmt.Sprintf("%f", userID) + "', '', '" + strconv.FormatInt(currentDateTime-2*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO comments (post, user, body, datetime) VALUES ('" + strconv.Itoa(postID) + "', '" + fmt.Sprintf("%f", userID) + "', '', '" + strconv.FormatInt(currentDateTime-10*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}

	// Sample data struct
	var (
		SampleData = []struct {
			userID   float64
			days     int
			comments int
		}{
			{
				userID:   userID,
				days:     3,
				comments: 2,
			},
			{
				userID:   userID,
				days:     7,
				comments: 2,
			},
			{
				userID:   userID,
				days:     15,
				comments: 3,
			},
			{
				userID:   userID,
				days:     30,
				comments: 3,
			},
		}
	)

	// Test function
	for _, sd := range SampleData {
		comments := GetCommentsNumByUser(sd.userID, sd.days)
		if comments != sd.comments {
			t.Errorf(`"%s" => unexpected error: "%v"`, strconv.Itoa(sd.days), "comments number do not match")
		}
	}

	// Delete test user
	rows, err := db.Prepare("DELETE FROM users WHERE id = ?")
	if err == nil {
		exec, err := rows.Exec(userID)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	// Delete test post
	rows, err = db.Prepare("DELETE FROM posts WHERE id = ?")
	if err == nil {
		exec, err := rows.Exec(postID)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	// Delete test comments
	rows, err = db.Prepare("DELETE FROM comments WHERE user = ?")
	if err == nil {
		exec, err := rows.Exec(userID)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
}
