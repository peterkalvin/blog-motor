package postrepository

import (
	sqlite "blog-motor/Repositories/SQLite"
	"fmt"
	"time"
)

// GetUser - Get the uploader user ID
// @param {int} - Post ID
// @return {float64} - User ID
// @return {error}
func GetUser(postID int) (float64, error) {
	db := sqlite.DB
	rows, err := db.Query("SELECT user FROM posts WHERE id = ?", postID)
	if err == nil {
		for rows.Next() {
			var userID float64
			err = rows.Scan(&userID)
			if err == nil {
				rows.Close()
				return userID, nil
			}
			// If scan can't run
			rows.Close()
			return 0, err
		}
	}
	// If the query can't run
	return 0, err
}

// GetLastByUser - Get last post ID
// @param {float64} - User ID
// @return {int} - Post ID
// @return {error}
func GetLastByUser(userID float64) (int, error) {
	db := sqlite.DB
	rows, err := db.Query("SELECT id FROM posts WHERE user = ? ORDER BY id DESC", fmt.Sprintf("%f", userID))
	if err == nil {
		for rows.Next() {
			var id int
			err = rows.Scan(&id)
			if err == nil {
				rows.Close()
				return id, nil
			}
			// If scan can't run
			rows.Close()
			return 0, err
		}
	}
	// If the query can't run
	return 0, err
}

// GetPostsNumByUser - Get posts number
// @param {float64} - User ID
// @param {int} - Days
// @return {int} - Posts number
func GetPostsNumByUser(userID float64, days int) int {
	// Get current date and time in unix
	currentDateTime := time.Now().Unix()
	db := sqlite.DB
	rows, err := db.Query("SELECT datetime FROM posts WHERE user = ?", fmt.Sprintf("%f", userID))
	if err == nil {
		var posts []int
		for rows.Next() {
			var datetime int
			err = rows.Scan(&datetime)
			if err == nil {
				if datetime > int(currentDateTime)-days*84000 {
					posts = append(posts, datetime)
				}
			} else {
				// If scan can't run
				rows.Close()
				return 0
			}
		}
		rows.Close()
		return len(posts)
	}
	// If the query can't run
	return 0
}
