package postrepository

import (
	sqlite "blog-motor/Repositories/SQLite"
	"fmt"
	"strconv"
	"testing"
	"time"

	userrepository "blog-motor/Repositories/Users"

	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/crypto/bcrypt"
)

// TestGetUser - Testing the GetUser function
func TestGetUser(t *testing.T) {
	// Setup database connection
	sqlite.ConnectSQLite("../../")
	db := sqlite.DB

	// Create password hash
	hash, _ := bcrypt.GenerateFromPassword([]byte("1234"), 10)
	// Upload test user
	email := "teszt@teszt.hu"
	_, err := db.Exec("INSERT INTO users (email, password) VALUES ('" + email + "', '" + string(hash) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	userID, _ := userrepository.GetUserByEmail(email)

	// Upload test post
	currentDateTime := time.Now().Unix()
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID) + "', '', '', '" + strconv.FormatInt(currentDateTime, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	postID, _ := GetLastByUser(userID)

	// Test function
	uploaderID, err := GetUser(postID)
	if err != nil {
		t.Errorf(`"%s" => unexpected error: "%v"`, strconv.Itoa(postID), err)
	} else if uploaderID != userID {
		t.Errorf(`"%s" => unexpected error: "%v"`, strconv.Itoa(postID), "User IDs not match")
	}

	// Delete test user
	rows, err := db.Prepare("DELETE FROM users WHERE id = ?")
	if err == nil {
		exec, err := rows.Exec(userID)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	// Delete test post
	rows, err = db.Prepare("DELETE FROM posts WHERE id = ?")
	if err == nil {
		exec, err := rows.Exec(postID)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
}

// TestGetPostsNumByUser - Testing the GetPostsNumByUser function
func TestGetPostsNumByUser(t *testing.T) {
	// Setup database connection
	sqlite.ConnectSQLite("../../")
	db := sqlite.DB

	// Create password hash
	hash, _ := bcrypt.GenerateFromPassword([]byte("1234"), 10)
	// Upload test user
	email := "teszt@teszt.hu"
	_, err := db.Exec("INSERT INTO users (email, password) VALUES ('" + email + "', '" + string(hash) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	userID, _ := userrepository.GetUserByEmail(email)

	// Upload test posts
	currentDateTime := time.Now().Unix()
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID) + "', '', '', '" + strconv.FormatInt(currentDateTime, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID) + "', '', '', '" + strconv.FormatInt(currentDateTime-5*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID) + "', '', '', '" + strconv.FormatInt(currentDateTime-20*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}

	// Sample data struct
	var (
		SampleData = []struct {
			userID float64
			days   int
			posts  int
		}{
			{
				userID: userID,
				days:   3,
				posts:  1,
			},
			{
				userID: userID,
				days:   7,
				posts:  2,
			},
			{
				userID: userID,
				days:   15,
				posts:  2,
			},
			{
				userID: userID,
				days:   30,
				posts:  3,
			},
		}
	)

	// Test function
	for _, sd := range SampleData {
		posts := GetPostsNumByUser(sd.userID, sd.days)
		if posts != sd.posts {
			t.Errorf(`"%s" => unexpected error: "%v"`, strconv.Itoa(sd.days), "posts number do not match")
		}
	}

	// Delete test user
	rows, err := db.Prepare("DELETE FROM users WHERE id = ?")
	if err == nil {
		exec, err := rows.Exec(userID)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	// Delete test posts
	rows, err = db.Prepare("DELETE FROM posts WHERE user = ?")
	if err == nil {
		exec, err := rows.Exec(userID)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
}
