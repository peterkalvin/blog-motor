package configtests

// Envs - Environment settings struct
type Envs struct {
	Environment         string `json:"Environment"`         // Environment value set in OS
	ExpectedEnvironment string `json:"ExpectedEnvironment"` // Expected environment value
	ExpectedMatch       bool   `json:"ExpectedMatch"`       // Expected match with environment value
}

// TestConfigs - Struct for config testing purposes
type TestConfigs struct {
	Type string `json:"Type"` // Config type
	Key  string `json:"Key"`  // Key for config
}
