package urltests

// URLTest - Struct for API testing purposes
type URLTest struct {
	MethodType           string `json:"MethodType"` // The type of the given method
	BearerToken          string // Bearer token
	URL                  string `json:"URL"`                            // The URL of the query eg.: /test
	RequestBody          string `json:"ExpectedRequestBody,omitempty"`  // The expected body of the request in json string
	ExpectedStatusCode   int    `json:"ExpectedStatusCode"`             // The expected status code eg: 200
	ExpectedResponseBody string `json:"ExpectedResponseBody,omitempty"` // The expected body of the response message in json string
}
