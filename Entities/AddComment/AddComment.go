package addcomment

// Request - Struct of the request
type Request struct {
	PostID int    `json:"PostID"` // Post ID
	Body   string `json:"Body"`   // Comment body
}
