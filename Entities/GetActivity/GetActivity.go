package getactivity

// Activity - Struct of the user's activity
type Activity struct {
	UserID   float64 // User ID
	Posts    int     // Posts number
	Comments int     // Comments number
}

// Response - Struct of the response
type Response struct {
	Activities []interface{} // Activities
}
