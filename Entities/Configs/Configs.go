package configs

// Environment - Environment selectors struct
type Environment struct {
	LocalEnvironmentVariable       string `json:"LocalEnvironmentVariable"`       // Local variable
	LocalEnvironmentValue          string `json:"LocalEnvironmentValue"`          // Local variable value
	DevelopmentEnvironmentVariable string `json:"DevelopmentEnvironmentVariable"` // Development variable
	DevelopmentEnvironmentValue    string `json:"DevelopmentEnvironmentValue"`    // Development variable value
	ProductionEnvironmentVariable  string `json:"ProductionEnvironmentVariable"`  // Production variable
	ProductionEnvironmentValue     string `json:"ProductionEnvironmentValue"`     // Production variable value
}

// API - API settings struct
type API struct {
	ListeningPort int32 `json:"ApiListeningPort"` // Listening port
}

// Database - Database settings struct
type Database struct {
	Type string `json:"DatabaseType"` // Database type
}

// External - Misc settings struct
type External struct {
	TokenKey string `json:"TokenKey,omitempty"` // Token key
}

// AllConfig - Contains all configurations loaded in a single struct
type AllConfig struct {
	API      API      `json:"api"`
	Database Database `json:"database"`
	External External `json:"external"`
}
