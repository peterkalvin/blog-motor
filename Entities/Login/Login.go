package login

// Request - Struct of the request
type Request struct {
	Email    string `json:"Email"`    // Email address
	Password string `json:"Password"` // Password
}

// Response - Struct of the response
type Response struct {
	Bearer string // Bearer token
}
