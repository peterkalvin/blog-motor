package main

import (
	api "blog-motor/Deliveries/API"
	sqlite "blog-motor/Repositories/SQLite"
	configs "blog-motor/UseCases/Configs"

	"github.com/julienschmidt/httprouter"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	// Load configuration files
	configs.LoadConfigs("./")

	// Set up SQLite connection
	sqlite.ConnectSQLite("./")

	// Create new router
	router := httprouter.New()

	// Start new REST API
	api.NewAPI(router)
}
