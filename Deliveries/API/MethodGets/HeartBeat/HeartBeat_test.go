package get

import (
	"net/http"
	"net/http/httptest"
	"testing"

	urltests "blog-motor/Entities/URLTests"
)

// TestHeartBeat - Loading test
func TestHeartBeat(t *testing.T) {
	// Sample url
	URLTest := urltests.URLTest{
		MethodType: "GET",
		URL:        "/",
	}

	// Test function
	request, err := http.NewRequest(URLTest.MethodType, "localhost:80"+URLTest.URL, nil)
	if err != nil {
		t.Fatalf("HTTP NOT WORKING")
	}
	rec := httptest.NewRecorder()
	HeartBeat(rec, request, nil)
	response := rec.Result()
	if response.StatusCode != http.StatusOK {
		t.Errorf(URLTest.URL + " not working")
	}

}
