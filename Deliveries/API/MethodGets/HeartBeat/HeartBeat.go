package get

import (
	log "blog-motor/UseCases/Log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// HeartBeat - Handle / request of the API
// @param {http.ResponseWriter} - Response writer
// @param {*http.Request} - Pointer of the request
// @param {httprouter.Params} - Parameters
func HeartBeat(httpResponseWriter http.ResponseWriter, httpRequest *http.Request, _ httprouter.Params) {
	httpResponseWriter.WriteHeader(200)
	log.Log("info", "HeartBeat: 200 OK")
}
