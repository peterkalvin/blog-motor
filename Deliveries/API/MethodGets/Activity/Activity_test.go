package get

import (
	urltests "blog-motor/Entities/URLTests"
	sqlite "blog-motor/Repositories/SQLite"
	configs "blog-motor/UseCases/Configs"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
	"time"

	postrepository "blog-motor/Repositories/Posts"
	userrepository "blog-motor/Repositories/Users"

	"github.com/julienschmidt/httprouter"
	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/crypto/bcrypt"
)

// TestGetActivity - Testing the GetActivity function
func TestGetActivity(t *testing.T) {
	// Setup database connection
	sqlite.ConnectSQLite("../../../../")
	db := sqlite.DB

	// Create password hash
	hash, _ := bcrypt.GenerateFromPassword([]byte("1234"), 10)
	// Upload test users
	email := "teszt@teszt.hu"
	_, err := db.Exec("INSERT INTO users (email, password) VALUES ('" + email + "', '" + string(hash) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	userID1, _ := userrepository.GetUserByEmail(email)
	email = "teszt@teszt.com"
	_, err = db.Exec("INSERT INTO users (email, password) VALUES ('" + email + "', '" + string(hash) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	userID2, _ := userrepository.GetUserByEmail(email)

	// Upload test posts
	currentDateTime := time.Now().Unix()
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID1) + "', '', '', '" + strconv.FormatInt(currentDateTime, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID1) + "', '', '', '" + strconv.FormatInt(currentDateTime-5*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID1) + "', '', '', '" + strconv.FormatInt(currentDateTime-20*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	postID1, _ := postrepository.GetLastByUser(userID1)
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID2) + "', '', '', '" + strconv.FormatInt(currentDateTime, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	postID2, _ := postrepository.GetLastByUser(userID2)

	// Upload test comments
	_, err = db.Exec("INSERT INTO comments (post, user, body, datetime) VALUES ('" + strconv.Itoa(postID1) + "', '" + fmt.Sprintf("%f", userID1) + "', '', '" + strconv.FormatInt(currentDateTime, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO comments (post, user, body, datetime) VALUES ('" + strconv.Itoa(postID1) + "', '" + fmt.Sprintf("%f", userID1) + "', '', '" + strconv.FormatInt(currentDateTime-2*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO comments (post, user, body, datetime) VALUES ('" + strconv.Itoa(postID1) + "', '" + fmt.Sprintf("%f", userID1) + "', '', '" + strconv.FormatInt(currentDateTime-10*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO comments (post, user, body, datetime) VALUES ('" + strconv.Itoa(postID2) + "', '" + fmt.Sprintf("%f", userID2) + "', '', '" + strconv.FormatInt(currentDateTime-2*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO comments (post, user, body, datetime) VALUES ('" + strconv.Itoa(postID2) + "', '" + fmt.Sprintf("%f", userID2) + "', '', '" + strconv.FormatInt(currentDateTime-10*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}

	// Create test days slice
	days := []string{"3", "7", "15", "30"}

	// Create url test data
	URLTests := []urltests.URLTest{
		{
			MethodType:           "GET",
			URL:                  "/activity",
			ExpectedStatusCode:   200,
			ExpectedResponseBody: "{\"Activities\":[{\"UserID\":" + strconv.Itoa(int(userID1)) + ",\"Posts\":1,\"Comments\":2},{\"UserID\":" + strconv.Itoa(int(userID2)) + ",\"Posts\":1,\"Comments\":1}]}",
		},
		{
			MethodType:           "GET",
			URL:                  "/activity",
			ExpectedStatusCode:   200,
			ExpectedResponseBody: "{\"Activities\":[{\"UserID\":" + strconv.Itoa(int(userID1)) + ",\"Posts\":2,\"Comments\":2},{\"UserID\":" + strconv.Itoa(int(userID2)) + ",\"Posts\":1,\"Comments\":1}]}",
		},
		{
			MethodType:           "GET",
			URL:                  "/activity",
			ExpectedStatusCode:   200,
			ExpectedResponseBody: "{\"Activities\":[{\"UserID\":" + strconv.Itoa(int(userID1)) + ",\"Posts\":2,\"Comments\":3},{\"UserID\":" + strconv.Itoa(int(userID2)) + ",\"Posts\":1,\"Comments\":2}]}",
		},
		{
			MethodType:           "GET",
			URL:                  "/activity",
			ExpectedStatusCode:   200,
			ExpectedResponseBody: "{\"Activities\":[{\"UserID\":" + strconv.Itoa(int(userID1)) + ",\"Posts\":3,\"Comments\":3},{\"UserID\":" + strconv.Itoa(int(userID2)) + ",\"Posts\":1,\"Comments\":2}]}",
		},
	}

	// Setup listening port
	listeningPort, err := configs.GetInt("API", "ListeningPort")
	if err != nil {
		listeningPort = 80
	}

	// Test function with urls
	for i, URLTest := range URLTests {
		body := strings.NewReader("")
		request, err := http.NewRequest(URLTest.MethodType, "localhost:"+string(listeningPort)+URLTest.URL, body)
		if err != nil {
			t.Fatalf("HTTP NOT WORKING")
		}
		rec := httptest.NewRecorder()
		// Add params
		var params httprouter.Params
		params = append(params, httprouter.Param{Key: "days", Value: days[i]})
		Activity(rec, request, params)
		response := rec.Result()
		if response.StatusCode != URLTest.ExpectedStatusCode {
			t.Errorf(URLTest.URL + " status mismatch")
		}
		responseBody, err := ioutil.ReadAll(response.Body)
		defer response.Body.Close()
		if err != nil {
			t.Errorf(URLTest.URL + " cant read response")
		} else {
			if strings.TrimSpace(string(responseBody)) != URLTest.ExpectedResponseBody {
				t.Errorf(URLTest.URL + " response mismatch - have: " + string(responseBody) + " want: " + URLTest.ExpectedResponseBody)
			}
		}
	}

	// Delete test users
	rows, err := db.Prepare("DELETE FROM users WHERE id = ?")
	if err == nil {
		exec, err := rows.Exec(userID1)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	rows, err = db.Prepare("DELETE FROM users WHERE id = ?")
	if err == nil {
		exec, err := rows.Exec(userID2)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	// Delete test posts
	rows, err = db.Prepare("DELETE FROM posts WHERE user = ?")
	if err == nil {
		exec, err := rows.Exec(userID1)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	rows, err = db.Prepare("DELETE FROM posts WHERE user = ?")
	if err == nil {
		exec, err := rows.Exec(userID2)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	// Delete test comments
	rows, err = db.Prepare("DELETE FROM comments WHERE user = ?")
	if err == nil {
		exec, err := rows.Exec(userID1)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	rows, err = db.Prepare("DELETE FROM comments WHERE user = ?")
	if err == nil {
		exec, err := rows.Exec(userID2)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
}
