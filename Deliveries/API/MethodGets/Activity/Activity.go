package get

import (
	entity "blog-motor/Entities/GetActivity"
	usecase "blog-motor/UseCases/GetActivity"
	"encoding/json"
	"net/http"
	"strconv"

	log "blog-motor/UseCases/Log"

	"github.com/julienschmidt/httprouter"
)

// The response struct
type responseStruct entity.Response

// Activity - Get user activity
// @param {http.ResponseWriter} - Response writer
// @param {*http.Request} - Pointer of the request
// @param {httprouter.Params} - Parameters
func Activity(httpResponseWriter http.ResponseWriter, httpRequest *http.Request, httpParams httprouter.Params) {
	// Get the days
	days, _ := strconv.Atoi(httpParams.ByName("days"))
	// Actual GetActivity usecase
	activities, err := usecase.GetActivity(days)
	if err != nil {
		http.Error(httpResponseWriter, err.Error(), 400)
		log.Log("error", "Activity: 400 ERROR ("+err.Error()+")")
		return
	}
	// Preparing response data
	var output []byte
	response := &responseStruct{activities}
	output, err = json.Marshal(response)
	if err != nil {
		http.Error(httpResponseWriter, err.Error(), 400)
		log.Log("error", "Activity: 400 ERROR ("+err.Error()+")")
		return
	}
	// Sending response to client
	httpResponseWriter.Header().Set("Content-Type", "application/json")
	httpResponseWriter.Write(output)
	log.Log("info", "Activity: 200 OK")
}
