package post

import (
	entity "blog-motor/Entities/Login"
	log "blog-motor/UseCases/Log"
	usecase "blog-motor/UseCases/Login"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// The response struct
type responseStruct entity.Response

// Login - Login the user
// @param {http.ResponseWriter} - Response writer
// @param {*http.Request} - Pointer of the request
// @param {httprouter.Params} - Parameters
func Login(httpResponseWriter http.ResponseWriter, httpRequest *http.Request, _ httprouter.Params) {
	// Setting up CORS headers
	httpResponseWriter.Header().Set("Access-Control-Allow-Origin", "*")
	httpResponseWriter.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	// Get the body
	body, err := ioutil.ReadAll(httpRequest.Body)
	defer httpRequest.Body.Close()
	if err != nil {
		http.Error(httpResponseWriter, "Bad request", 400)
		log.Log("error", "Login: 400 ERROR (Bad request)")
		return
	}
	// Handle request's body message
	var requestStruct usecase.Struct
	err = json.Unmarshal(body, &requestStruct)
	if err != nil {
		http.Error(httpResponseWriter, "Incorrect request", 400)
		log.Log("error", "Login: 400 ERROR (Incorrect request)")
		return
	}
	// Actual Login usecase
	bearer, err := usecase.Login(&requestStruct)
	if err != nil {
		http.Error(httpResponseWriter, err.Error(), 400)
		log.Log("error", "Login: 400 ERROR ("+err.Error()+")")
		return
	}
	// Preparing response data
	var output []byte
	response := &responseStruct{bearer}
	output, err = json.Marshal(response)
	if err != nil {
		http.Error(httpResponseWriter, err.Error(), 400)
		log.Log("error", "Login: 400 ERROR ("+err.Error()+")")
		return
	}
	// Sending response to client
	httpResponseWriter.Header().Set("Content-Type", "application/json")
	httpResponseWriter.Write(output)
	log.Log("info", "Login: 200 OK")
}
