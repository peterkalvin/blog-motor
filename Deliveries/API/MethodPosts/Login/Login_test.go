package post

import (
	urltests "blog-motor/Entities/URLTests"
	sqlite "blog-motor/Repositories/SQLite"
	configs "blog-motor/UseCases/Configs"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/crypto/bcrypt"
)

// TestLogin - Testing the Login function
func TestLogin(t *testing.T) {
	// Setup database connection
	sqlite.ConnectSQLite("../../../../")
	db := sqlite.DB

	// Create password hash
	hash, _ := bcrypt.GenerateFromPassword([]byte("1234"), 10)
	// Upload test user
	email := "teszt@teszt.hu"
	_, err := db.Exec("INSERT INTO users (email, password) VALUES ('" + email + "', '" + string(hash) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}

	// Create url test data
	URLTests := []urltests.URLTest{
		{
			MethodType:         "POST",
			URL:                "/login",
			RequestBody:        "{\"Email\": \"" + email + "\", \"Password\": \"1234\"}",
			ExpectedStatusCode: 200,
		},
		{
			MethodType:           "POST",
			URL:                  "/login",
			RequestBody:          "{\"Email\": \"" + email + "\", \"Password\": \"2345\"}",
			ExpectedStatusCode:   400,
			ExpectedResponseBody: "Bad password",
		},
		{
			MethodType:           "POST",
			URL:                  "/login",
			RequestBody:          "{\"Email\": \"teszt@teszt.com\", \"Password\": \"1234\"}",
			ExpectedStatusCode:   400,
			ExpectedResponseBody: "Bad password",
		},
	}

	// Setup listening port
	listeningPort, err := configs.GetInt("API", "ListeningPort")
	if err != nil {
		listeningPort = 80
	}

	// Test function with urls
	for _, URLTest := range URLTests {
		body := strings.NewReader(URLTest.RequestBody)
		request, err := http.NewRequest(URLTest.MethodType, "localhost:"+string(listeningPort)+URLTest.URL, body)
		if err != nil {
			t.Fatalf("HTTP NOT WORKING")
		}
		rec := httptest.NewRecorder()
		Login(rec, request, nil)
		response := rec.Result()
		if response.StatusCode != URLTest.ExpectedStatusCode {
			t.Errorf(URLTest.URL + " status mismatch")
		}
		if response.StatusCode != 200 {
			responseBody, err := ioutil.ReadAll(response.Body)
			defer response.Body.Close()
			if err != nil {
				t.Errorf(URLTest.URL + " cant read response")
			} else {
				if strings.TrimSpace(string(responseBody)) != URLTest.ExpectedResponseBody {
					t.Errorf(URLTest.URL + " response mismatch - have: " + string(responseBody) + " want: " + URLTest.ExpectedResponseBody)
				}
			}
		}
	}

	// Delete test user
	rows, err := db.Prepare("DELETE FROM users WHERE email = ?")
	if err == nil {
		exec, err := rows.Exec(email)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
}
