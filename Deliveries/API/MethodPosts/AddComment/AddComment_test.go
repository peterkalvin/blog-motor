package post

import (
	urltests "blog-motor/Entities/URLTests"
	sqlite "blog-motor/Repositories/SQLite"
	configs "blog-motor/UseCases/Configs"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
	"time"

	postrepository "blog-motor/Repositories/Posts"
	userrepository "blog-motor/Repositories/Users"

	jwt "github.com/dgrijalva/jwt-go"
	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/crypto/bcrypt"
)

// TestAddComment - Testing the AddComment function
func TestAddComment(t *testing.T) {
	// Setup database connection
	sqlite.ConnectSQLite("../../../../")
	db := sqlite.DB

	// Create password hash
	hash, _ := bcrypt.GenerateFromPassword([]byte("1234"), 10)
	// Upload test user
	email := "teszt@teszt.hu"
	_, err := db.Exec("INSERT INTO users (email, password) VALUES ('" + email + "', '" + string(hash) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	userID, _ := userrepository.GetUserByEmail(email)

	// Upload test post
	currentDateTime := time.Now().Unix()
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID) + "', '', '', '" + strconv.FormatInt(currentDateTime, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	postID, _ := postrepository.GetLastByUser(userID)

	// Create token for test user
	token := jwt.New(jwt.SigningMethodHS512)
	claims := make(jwt.MapClaims)
	claims["id"] = userID
	token.Claims = claims
	tokenKey, _ := configs.GetString("External", "TokenKey")
	signedToken, _ := token.SignedString([]byte(tokenKey))

	// Create url test data
	URLTests := []urltests.URLTest{
		{
			MethodType:         "POST",
			URL:                "/add-comment",
			BearerToken:        "Bearer " + signedToken,
			RequestBody:        "{\"PostID\": " + strconv.Itoa(postID) + ", \"Body\": \"Comment\"}",
			ExpectedStatusCode: 200,
		},
		{
			MethodType:           "POST",
			URL:                  "/add-comment",
			BearerToken:          "Bearer " + signedToken,
			RequestBody:          "{\"PostID\": " + strconv.Itoa(postID+1) + ", \"Body\": \"Comment\"}",
			ExpectedStatusCode:   400,
			ExpectedResponseBody: "No access",
		},
		{
			MethodType:           "POST",
			URL:                  "/add-comment",
			BearerToken:          "Bearer 12345",
			RequestBody:          "{\"PostID\": " + strconv.Itoa(postID) + ", \"Body\": \"Comment\"}",
			ExpectedStatusCode:   400,
			ExpectedResponseBody: "Bad token",
		},
		{
			MethodType:           "POST",
			URL:                  "/add-comment",
			BearerToken:          signedToken,
			RequestBody:          "{\"PostID\": " + strconv.Itoa(postID) + ", \"Body\": \"Comment\"}",
			ExpectedStatusCode:   400,
			ExpectedResponseBody: "Token parsing error",
		},
	}

	// Setup listening port
	listeningPort, err := configs.GetInt("API", "ListeningPort")
	if err != nil {
		listeningPort = 80
	}

	// Test function with urls
	for _, URLTest := range URLTests {
		body := strings.NewReader(URLTest.RequestBody)
		request, err := http.NewRequest(URLTest.MethodType, "localhost:"+string(listeningPort)+URLTest.URL, body)
		if err != nil {
			t.Fatalf("HTTP NOT WORKING")
		}
		request.Header.Set("bearer", URLTest.BearerToken)
		rec := httptest.NewRecorder()
		AddComment(rec, request, nil)
		response := rec.Result()
		if response.StatusCode != URLTest.ExpectedStatusCode {
			t.Errorf(URLTest.URL + " status mismatch")
		}
		if response.StatusCode != 200 {
			responseBody, err := ioutil.ReadAll(response.Body)
			defer response.Body.Close()
			if err != nil {
				t.Errorf(URLTest.URL + " cant read response")
			} else {
				if strings.TrimSpace(string(responseBody)) != URLTest.ExpectedResponseBody {
					t.Errorf(URLTest.URL + " response mismatch - have: " + string(responseBody) + " want: " + URLTest.ExpectedResponseBody)
				}
			}
		}
	}

	// Delete test user
	rows, err := db.Prepare("DELETE FROM users WHERE id = ?")
	if err == nil {
		exec, err := rows.Exec(userID)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	// Delete test post
	rows, err = db.Prepare("DELETE FROM posts WHERE id = ?")
	if err == nil {
		exec, err := rows.Exec(postID)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	// Delete test comment
	rows, err = db.Prepare("DELETE FROM comments WHERE user = ?")
	if err == nil {
		exec, err := rows.Exec(userID)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
}
