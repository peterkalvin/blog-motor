package post

import (
	usecase "blog-motor/UseCases/AddComment"
	log "blog-motor/UseCases/Log"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// AddComment - Add comment to post
// @param {http.ResponseWriter} - Response writer
// @param {*http.Request} - Pointer of the request
// @param {httprouter.Params} - Parameters
func AddComment(httpResponseWriter http.ResponseWriter, httpRequest *http.Request, _ httprouter.Params) {
	// Setting up CORS headers
	httpResponseWriter.Header().Set("Access-Control-Allow-Origin", "*")
	httpResponseWriter.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	// Get the bearer token
	bearer := httpRequest.Header.Get("bearer")
	// Get the body
	body, err := ioutil.ReadAll(httpRequest.Body)
	defer httpRequest.Body.Close()
	if err != nil {
		http.Error(httpResponseWriter, "Bad request", 400)
		log.Log("error", "AddComment: 400 ERROR (Bad request)")
		return
	}
	// Handle request's body message
	var requestStruct usecase.Struct
	err = json.Unmarshal(body, &requestStruct)
	if err != nil {
		http.Error(httpResponseWriter, "Incorrect request", 400)
		log.Log("error", "AddComment: 400 ERROR (Incorrect request)")
		return
	}
	// Actual AddComment usecase
	err = usecase.AddComment(bearer, &requestStruct)
	if err != nil {
		http.Error(httpResponseWriter, err.Error(), 400)
		log.Log("error", "AddComment: 400 ERROR ("+err.Error()+")")
		return
	}
	log.Log("info", "AddComment: 200 OK")
}
