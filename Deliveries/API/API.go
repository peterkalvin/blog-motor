package api

import (
	configs "blog-motor/UseCases/Configs"
	"fmt"
	"net/http"
	"strconv"

	activity "blog-motor/Deliveries/API/MethodGets/Activity"
	heartbeat "blog-motor/Deliveries/API/MethodGets/HeartBeat"
	addcomment "blog-motor/Deliveries/API/MethodPosts/AddComment"
	login "blog-motor/Deliveries/API/MethodPosts/Login"

	log "blog-motor/UseCases/Log"

	"github.com/julienschmidt/httprouter"
)

// NewAPI - Create and handle request from REST API
func NewAPI(r *httprouter.Router) {
	// Parse requests
	parseGetRequests(r)
	parsePostRequests(r)
	// Get listening port from configs
	listeningPort, err := configs.GetInt("API", "ListeningPort")
	fmt.Printf("Listening on: %d"+"\n", listeningPort)
	if err == nil {
		listenPort := ":" + strconv.FormatInt(int64(listeningPort), 10)
		// Listen and serve
		log.Log("error", "API: "+http.ListenAndServe(listenPort, r).Error())
	}
}

// parseGetRequests - Parse GET requests from REST API
func parseGetRequests(r *httprouter.Router) {
	r.GET("/heart-beat", heartbeat.HeartBeat)
	r.GET("/activity/:days", activity.Activity)
}

// parsePostRequests - Parse POST requests from REST API
func parsePostRequests(r *httprouter.Router) {
	r.POST("/login", login.Login)
	r.POST("/add-comment", addcomment.AddComment)
}
