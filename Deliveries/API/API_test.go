package api

import (
	"net/http"
	"net/http/httptest"
	"testing"

	heartbeat "blog-motor/Deliveries/API/MethodGets/HeartBeat"

	urltests "blog-motor/Entities/URLTests"
)

// TestNewAPI - Testing the NewAPI function
func TestNewAPI(t *testing.T) {

	// Sample url
	URLTest := urltests.URLTest{
		MethodType: "GET",
		URL:        "/",
	}

	// Test function
	request, err := http.NewRequest(URLTest.MethodType, "localhost:80"+URLTest.URL, nil)
	if err != nil {
		t.Fatalf("HTTP NOT WORKING")
	}
	rec := httptest.NewRecorder()
	heartbeat.HeartBeat(rec, request, nil)
	response := rec.Result()
	if response.StatusCode != http.StatusOK {
		t.Errorf(URLTest.URL + " not working")
	}

}
