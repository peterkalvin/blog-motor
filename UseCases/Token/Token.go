package token

import (
	configs "blog-motor/UseCases/Configs"
	"errors"
	"fmt"
	"strings"

	jwt "github.com/dgrijalva/jwt-go"
)

// ParseBearerToken - Get bearer token from the header bearer string
// @param {string} - Bearer token
// @return {string} - Token key
// @return {error}
func ParseBearerToken(bearerToken string) (token string, err error) {
	// Split token string
	stringArray := strings.Split(bearerToken, "Bearer")
	// Check string length
	if len(stringArray) > 1 {
		return strings.TrimSpace(stringArray[1]), nil
	}
	return "", errors.New("Token parsing error")
}

// SolveToken -  Validates token from string
// @param {string} - Token key
// @return {map[string]interface{}} - Token data
// @return {error}
func SolveToken(tokenString string) (map[string]interface{}, error) {
	// Parse token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Check token signing
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		// Get the token key from config
		tokenKey, err := configs.GetString("External", "TokenKey")
		if err != nil {
			return "", err
		}
		return []byte(tokenKey), nil
	})
	if err != nil {
		return nil, errors.New("Bad token")
	}
	// Check token claims
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		_, ok := claims["id"]
		if !ok {
			return nil, errors.New("Bad token")
		}
		return claims, nil
	}
	return nil, errors.New("Bad token")
}
