package token

import (
	configs "blog-motor/UseCases/Configs"
	"testing"

	jwt "github.com/dgrijalva/jwt-go"
)

// TestParseBearerToken  - Testing the ParseBearerToken function
func TestParseBearerToken(t *testing.T) {
	// Sample data
	var (
		SampleBearerTokens = []struct {
			BearerTokenString string
			TokenKey          string
		}{
			{
				BearerTokenString: "Bearer 123456789",
				TokenKey:          "123456789",
			},
			{
				BearerTokenString: " Bearer 123456789 ",
				TokenKey:          "123456789",
			},
			{
				BearerTokenString: " Bearer ₱ⱤɆVłɆ₩₮ɆӾ₮ ",
				TokenKey:          "₱ⱤɆVłɆ₩₮ɆӾ₮",
			},
		}
	)

	// Test function
	for _, testData := range SampleBearerTokens {
		token, success := ParseBearerToken(testData.BearerTokenString)
		if success != nil {
			t.Errorf("TestParseBearerToken Error! The Success == false with this token (%s) ", token)
		}
		if token != testData.TokenKey {
			t.Errorf("TestParseBearerToken Error! The response (%s) is not equal to the key (%s) ", token, testData.TokenKey)
		}
	}
}

// TestSolveToken - Testing the SolveToken function
func TestSolveToken(t *testing.T) {
	// Load configuration files
	configs.LoadConfigs("../../")

	// Create test token string
	token := jwt.New(jwt.SigningMethodHS512)
	claims := make(jwt.MapClaims)
	claims["id"] = 1
	token.Claims = claims
	tokenKey, _ := configs.GetString("External", "TokenKey")
	signedToken, _ := token.SignedString([]byte(tokenKey))

	// Test function
	solvedToken, err := SolveToken(signedToken)
	if err == nil {
		userID := solvedToken["id"].(float64)
		if userID != 1 {
			t.Errorf(`Unexpected error: user ID from token is wrong`)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}

}
