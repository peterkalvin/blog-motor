package login

import (
	entity "blog-motor/Entities/Login"
	userrepository "blog-motor/Repositories/Users"
	configs "blog-motor/UseCases/Configs"
	"errors"

	jwt "github.com/dgrijalva/jwt-go"

	"golang.org/x/crypto/bcrypt"
)

// Struct - The imported request struct
type Struct entity.Request

// Login - The login process
// @param {*Struct} - Data from deliveries
// @return {string} - Bearer token
// @return {error}
func Login(loginStruct *Struct) (string, error) {
	// Get email and password from the struct
	email := loginStruct.Email
	password := loginStruct.Password
	// Get hash string and user ID from database
	hash, _ := userrepository.GetHashByEmail(email)
	userID, _ := userrepository.GetUserByEmail(email)
	// Check password
	return checkPassword(password, hash, userID)
}

// checkPassword - Check password is correct
// @param {string} - Password from request
// @param {string} - Hash string from database
// @param {float64} - User ID
// @return {string} - Bearer token
// @return {error}
func checkPassword(password, hash string, userID float64) (string, error) {
	// Compare hash and password
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err == nil {
		// Create token
		return createToken(userID), nil
	}
	return "", errors.New("Bad password")
}

// createToken - Create token for the user
// @param {float64} - User ID
// @return {string} - Bearer token
func createToken(userID float64) string {
	// Create new token
	token := jwt.New(jwt.SigningMethodHS512)
	// Create new token claims map
	claims := make(jwt.MapClaims)
	// Add user ID to claims
	claims["id"] = userID
	// Set claims for the token
	token.Claims = claims
	// Get the token key from configs
	tokenKey, _ := configs.GetString("External", "TokenKey")
	// Get the signed token string
	signedToken, _ := token.SignedString([]byte(tokenKey))
	// Return the bearer token
	return "Bearer " + signedToken
}
