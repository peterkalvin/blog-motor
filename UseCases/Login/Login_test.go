package login

import (
	sqlite "blog-motor/Repositories/SQLite"
	userrepository "blog-motor/Repositories/Users"
	"testing"

	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/crypto/bcrypt"
)

// TestDoLogin  - Testing the DoLogin function
func TestDoLogin(t *testing.T) {
	// Setup database connection
	sqlite.ConnectSQLite("../../")
	db := sqlite.DB

	// Create password hash
	hash, _ := bcrypt.GenerateFromPassword([]byte("1234"), 10)
	// Upload test user
	email := "teszt@teszt.hu"
	_, err := db.Exec("INSERT INTO users (email, password) VALUES ('" + email + "', '" + string(hash) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	userID, _ := userrepository.GetUserByEmail(email)

	// Sample data struct
	var (
		SampleData = []struct {
			username string
			password string
			err      string
		}{
			{
				username: "teszt@teszt.hu",
				password: "1234",
			},
			{
				username: "teszt@teszt.hu",
				password: "2345",
				err:      "Bad password",
			},
			{
				username: "teszt",
				password: "1234",
				err:      "Bad password",
			},
		}
	)

	// Test function
	for _, sd := range SampleData {
		loginStruct := &Struct{sd.username, sd.password}
		_, err := Login(loginStruct)
		if err != nil && err.Error() != sd.err {
			t.Errorf(`"%s" => unexpected error: "%v"`, sd.username, err)
		}
	}

	// Delete test user
	rows, err := db.Prepare("DELETE FROM users WHERE id = ?")
	if err == nil {
		exec, err := rows.Exec(userID)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
}
