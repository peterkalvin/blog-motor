package log

import logrus "github.com/Sirupsen/logrus"

// Log -
func Log(logType, message string) {
	Formatter := new(logrus.TextFormatter)
	Formatter.TimestampFormat = "2006-01-02 15:04:05"
	Formatter.FullTimestamp = true
	logrus.SetFormatter(Formatter)
	switch logType {
	case "info":
		logrus.Info(message)
		break
	case "error":
		logrus.Error(message)
		break
	}
}
