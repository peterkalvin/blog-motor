package addcomment

import (
	entity "blog-motor/Entities/AddComment"
	commentrepository "blog-motor/Repositories/Comments"
	postrepository "blog-motor/Repositories/Posts"
	token "blog-motor/UseCases/Token"
	"errors"
)

// Struct - the imported request struct
type Struct entity.Request

// AddComment - The add comment process
// @param {string} - Bearer token
// @param {*Struct} - Data from deliveries
// @return {error}
func AddComment(bearer string, addCommentStruct *Struct) error {
	// Parse token
	parse, err := token.ParseBearerToken(bearer)
	if err == nil {
		// Validate token
		solvedToken, err := token.SolveToken(parse)
		if err == nil {
			// Check user access
			return checkAccess(solvedToken, addCommentStruct)
		}
		return err
	}
	return err
}

// checkAccess - Check user access then save comment
// @param {map[string]interface{}} - Token data
// @param {*Struct} - Data from deliveries
// @return {error}
func checkAccess(token map[string]interface{}, addCommentStruct *Struct) error {
	// Get user ID
	userID := token["id"].(float64)
	// Get post ID and comment body from the struct
	postID := addCommentStruct.PostID
	body := addCommentStruct.Body
	// Get uploader user ID from database
	uploaderID, err := postrepository.GetUser(postID)
	if err == nil {
		// Check access
		if userID == uploaderID {
			// Save comment to database
			return commentrepository.AddComment(postID, userID, body)
		}
		return errors.New("No access")
	}
	return err
}
