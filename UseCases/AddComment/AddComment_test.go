package addcomment

import (
	sqlite "blog-motor/Repositories/SQLite"
	configs "blog-motor/UseCases/Configs"
	"fmt"
	"strconv"
	"testing"
	"time"

	postrepository "blog-motor/Repositories/Posts"
	userrepository "blog-motor/Repositories/Users"

	jwt "github.com/dgrijalva/jwt-go"
	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/crypto/bcrypt"
)

// TestAddComment  - Testing the AddComment function
func TestAddComment(t *testing.T) {
	// Setup database connection
	sqlite.ConnectSQLite("../../")
	db := sqlite.DB

	// Create password hash
	hash, _ := bcrypt.GenerateFromPassword([]byte("1234"), 10)
	// Upload test user
	email := "teszt@teszt.hu"
	_, err := db.Exec("INSERT INTO users (email, password) VALUES ('" + email + "', '" + string(hash) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	userID, _ := userrepository.GetUserByEmail(email)

	// Upload test post
	currentDateTime := time.Now().Unix()
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID) + "', '', '', '" + strconv.FormatInt(currentDateTime, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	postID, _ := postrepository.GetLastByUser(userID)

	// Create token for test user
	token := jwt.New(jwt.SigningMethodHS512)
	claims := make(jwt.MapClaims)
	claims["id"] = userID
	token.Claims = claims
	tokenKey, _ := configs.GetString("External", "TokenKey")
	signedToken, _ := token.SignedString([]byte(tokenKey))

	// Sample data struct
	var (
		SampleData = []struct {
			bearer string
			postID int
			err    string
		}{
			{
				bearer: "Bearer " + signedToken,
				postID: postID,
			},
			{
				bearer: signedToken,
				postID: postID,
				err:    "Token parsing error",
			},
			{
				bearer: "Bearer 12345",
				postID: postID,
				err:    "Bad token",
			},
			{
				bearer: "Bearer " + signedToken,
				postID: postID + 1,
				err:    "No access",
			},
		}
	)

	// Test function
	for _, sd := range SampleData {
		addCommentStruct := &Struct{sd.postID, ""}
		err := AddComment(sd.bearer, addCommentStruct)
		if err != nil && err.Error() != sd.err {
			t.Errorf(`"%s" => unexpected error: "%v"`, sd.bearer, err)
		}
	}

	// Delete test user
	rows, err := db.Prepare("DELETE FROM users WHERE id = ?")
	if err == nil {
		exec, err := rows.Exec(userID)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	// Delete test post
	rows, err = db.Prepare("DELETE FROM posts WHERE id = ?")
	if err == nil {
		exec, err := rows.Exec(postID)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	// Delete test comment
	rows, err = db.Prepare("DELETE FROM comments WHERE user = ?")
	if err == nil {
		exec, err := rows.Exec(userID)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
}
