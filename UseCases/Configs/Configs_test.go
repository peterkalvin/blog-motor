package configs

import (
	"os"
	"strconv"
	"testing"

	configtests "blog-motor/Entities/ConfigTests"
)

// TestGetCurrentEnv - Testing the GetCurrentEnv function
func TestGetCurrentEnv(t *testing.T) {

	// Sample data
	Envs := []configtests.Envs{
		{
			Environment:         "INCORRECT",
			ExpectedEnvironment: "Local",
			ExpectedMatch:       false,
		},
		{
			Environment:         "DEVELOPMENT",
			ExpectedEnvironment: "Development",
			ExpectedMatch:       true,
		},
		{
			Environment:         "PRODUCTION",
			ExpectedEnvironment: "Production",
			ExpectedMatch:       true,
		},
		{
			Environment:         "LOCAL",
			ExpectedEnvironment: "Local",
			ExpectedMatch:       true,
		},
	}

	// Get the current environment settings to set it back later
	currentEnv := os.Getenv("BLOGMOTOR")

	// Test function
	for _, EnvTest := range Envs {
		os.Setenv("BLOGMOTOR", EnvTest.Environment)
		returned, returnederror := GetCurrentEnv("../../")
		if returned != EnvTest.ExpectedEnvironment {
			t.Errorf(EnvTest.Environment + " incorrectly matched to " + returned)
		}
		if returnederror != EnvTest.ExpectedMatch {
			t.Errorf(EnvTest.Environment + " incorrectly flagged to " + strconv.FormatBool(returnederror))
		}
	}

	// Set back to current environment
	os.Setenv("BLOGMOTOR", currentEnv)
}

// TestLoadConfigs - Testing the LoadConfig function
func TestLoadConfigs(t *testing.T) {
	// Test function
	returned := LoadConfigs("../../")
	if !returned {
		t.Fatalf("Config load error")
	}
	if configuration.API.ListeningPort == 0 {
		t.Fatalf("API Config not loaded")
	}
}

// TestGetString - Testing the GetString function
func TestGetString(t *testing.T) {
	// Sample data
	Configs := []configtests.TestConfigs{
		{
			Key:  "Type",
			Type: "Database",
		},
	}

	// Load config files
	LoadConfigs("../../")

	// Test function
	for _, Config := range Configs {
		_, err := GetString(Config.Type, Config.Key)
		if err != nil {
			t.Errorf(Config.Type + " " + Config.Key + " is not of type String")
		}
	}
}

// TestGetInt - Testing the GetInt function
func TestGetInt(t *testing.T) {
	// Sample data
	Configs := []configtests.TestConfigs{
		{
			Key:  "ListeningPort",
			Type: "API",
		},
	}

	// Load config files
	LoadConfigs("../../")

	// Test function
	for _, Config := range Configs {
		_, err := GetInt(Config.Type, Config.Key)
		if err != nil {
			t.Errorf(Config.Type + " " + Config.Key + " is not of type Int32")
		}
	}
}
