package configs

// Config - Configuration interface
type Config interface {
	GetCurrentEnv(path string) string
	LoadConfigs(path string) bool
	GetString(configType string, configKey string) (string, bool)
	GetInt(configType string, configKey string) (int, bool)
	GetBool(configType string, configKey string) (bool, bool)
}
