package configs

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"

	entity "blog-motor/Entities/Configs"
)

var configuration entity.AllConfig

// GetCurrentEnv - Get the current system environment based on Env.json
// @param {string} - Path to Configs folder
// @return {string} - System environment
// @return {bool} - Environment matched correctly
func GetCurrentEnv(path string) (string, bool) {
	// Open Env.json
	file, err := os.Open(path + "Configs/Environment/Env.json")
	if err != nil {
		fmt.Println(err)
		// Fallback to Local
		return "Local", false
	}

	// Import environment struct
	environment := entity.Environment{}

	// Decode environment file
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&environment)
	if err != nil {
		fmt.Println(err)
		// Fallback to Local
		return "Local", false
	}

	// Check environment is Local
	envvalue := os.Getenv(environment.LocalEnvironmentVariable)
	if envvalue == environment.LocalEnvironmentValue {
		return "Local", true
	}

	// Check environment is Development
	envvalue = os.Getenv(environment.DevelopmentEnvironmentVariable)
	if envvalue == environment.DevelopmentEnvironmentValue {
		return "Development", true
	}

	// Check environment is Production
	envvalue = os.Getenv(environment.ProductionEnvironmentVariable)
	if envvalue == environment.ProductionEnvironmentValue {
		return "Production", true
	}

	// Fallback to Local
	return "Local", false // Fallback to local if no match
}

// LoadConfigs - Loads all configuration files into memory
// @param {string} - Path to Configs folder
// @return {bool} - Loaded correctly
func LoadConfigs(path string) bool {
	// Check environment
	environment, match := GetCurrentEnv(path)
	if !match {
		fmt.Println("WARNING: Environment not matched correctly, fallback to Local")
	}

	// Loads all files from Configs folder
	files, err := ioutil.ReadDir(path + "Configs/")
	if err != nil {
		fmt.Println(err)
		return false
	}

	// Import configuration struct
	configuration = entity.AllConfig{}

	for _, f := range files {
		// Open config file based on current environment
		file, err := os.Open(path + "Configs/" + f.Name() + "/" + environment + ".json")
		if err != nil {
			continue // Step over if file not exists
		}

		// Decode config file
		decoder := json.NewDecoder(file)

		// Decode config file to appropriate struct member
		var decodeErr error
		switch name := f.Name(); name {
		case "API":
			decodeErr = decoder.Decode(&configuration.API)
			break
		case "Database":
			decodeErr = decoder.Decode(&configuration.Database)
			break
		case "External":
			decodeErr = decoder.Decode(&configuration.External)
			break
		}

		if decodeErr != nil {
			fmt.Println(f.Name() + " Config load error")
			fmt.Println(err)
			return false
		}
		fmt.Println(f.Name() + " Config loaded")
	}

	return true
}

// GetString - Returns string type config values
// @param {string} - Type of config item
// @param {string} - Member of config item
// @return {string} - Value of requested config item
// @return {bool} - Loaded correctly
func GetString(configType string, configKey string) (string, error) {
	var value = getValue(configType, configKey)
	if value.Type().Name() == "string" {
		return string(value.String()), nil
	}
	err := errors.New(configType + " " + configKey + " is not of type String")
	return "", err
}

// GetInt - Returns integer type config values
// @param {string} - Type of config item
// @param {string} - Member of config item
// @return {int32} - Value of requested config item
// @return {bool} - Loaded correctly
func GetInt(configType string, configKey string) (int32, error) {
	var value = getValue(configType, configKey)
	if value.Type().Name() == "int32" {
		return int32(value.Int()), nil
	}
	err := errors.New(configType + " " + configKey + " is not of type Int32")
	return -1, err
}

// GetBool - Returns boolean type config values
// @param {string} - Type of config item
// @param {string} - Member of config item
// @return {bool} - Value of requested config item
// @return {bool} - Loaded correctly
func GetBool(configType string, configKey string) (bool, error) {
	var value = getValue(configType, configKey)
	if value.Type().Name() == "bool" {
		return bool(value.Bool()), nil
	}
	err := errors.New(configType + " " + configKey + " is not of type Bool")
	return false, err
}

// getValue - Returns config value as reflect.Value
// @param {string} - Type of config item
// @param {string} - Member of config item
// @return {reflect.Value} - Value of requested config item
func getValue(configType string, configKey string) reflect.Value {
	var value reflect.Value
	switch configType {
	case "API":
		value = reflect.Indirect(reflect.ValueOf(&configuration.API)).FieldByName(configKey)
	case "Database":
		value = reflect.Indirect(reflect.ValueOf(&configuration.Database)).FieldByName(configKey)
	case "External":
		value = reflect.Indirect(reflect.ValueOf(&configuration.External)).FieldByName(configKey)
	}
	return value
}
