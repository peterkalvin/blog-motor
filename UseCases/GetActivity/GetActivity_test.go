package getactivity

import (
	entity "blog-motor/Entities/GetActivity"
	sqlite "blog-motor/Repositories/SQLite"
	"fmt"
	"strconv"
	"testing"
	"time"

	postrepository "blog-motor/Repositories/Posts"
	userrepository "blog-motor/Repositories/Users"

	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/crypto/bcrypt"
)

// TestGetActivity  - Testing the GetActivity function
func TestGetActivity(t *testing.T) {
	// Setup database connection
	sqlite.ConnectSQLite("../../")
	db := sqlite.DB

	// Create password hash
	hash, _ := bcrypt.GenerateFromPassword([]byte("1234"), 10)
	// Upload test users
	email := "teszt@teszt.hu"
	_, err := db.Exec("INSERT INTO users (email, password) VALUES ('" + email + "', '" + string(hash) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	userID1, _ := userrepository.GetUserByEmail(email)
	email = "teszt@teszt.com"
	_, err = db.Exec("INSERT INTO users (email, password) VALUES ('" + email + "', '" + string(hash) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	userID2, _ := userrepository.GetUserByEmail(email)

	// Upload test posts
	currentDateTime := time.Now().Unix()
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID1) + "', '', '', '" + strconv.FormatInt(currentDateTime, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID1) + "', '', '', '" + strconv.FormatInt(currentDateTime-5*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID1) + "', '', '', '" + strconv.FormatInt(currentDateTime-20*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	postID1, _ := postrepository.GetLastByUser(userID1)
	_, err = db.Exec("INSERT INTO posts (user, title, body, datetime) VALUES ('" + fmt.Sprintf("%f", userID2) + "', '', '', '" + strconv.FormatInt(currentDateTime, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	postID2, _ := postrepository.GetLastByUser(userID2)

	// Upload test comments
	_, err = db.Exec("INSERT INTO comments (post, user, body, datetime) VALUES ('" + strconv.Itoa(postID1) + "', '" + fmt.Sprintf("%f", userID1) + "', '', '" + strconv.FormatInt(currentDateTime, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO comments (post, user, body, datetime) VALUES ('" + strconv.Itoa(postID1) + "', '" + fmt.Sprintf("%f", userID1) + "', '', '" + strconv.FormatInt(currentDateTime-2*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO comments (post, user, body, datetime) VALUES ('" + strconv.Itoa(postID1) + "', '" + fmt.Sprintf("%f", userID1) + "', '', '" + strconv.FormatInt(currentDateTime-10*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO comments (post, user, body, datetime) VALUES ('" + strconv.Itoa(postID2) + "', '" + fmt.Sprintf("%f", userID2) + "', '', '" + strconv.FormatInt(currentDateTime-2*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	_, err = db.Exec("INSERT INTO comments (post, user, body, datetime) VALUES ('" + strconv.Itoa(postID2) + "', '" + fmt.Sprintf("%f", userID2) + "', '', '" + strconv.FormatInt(currentDateTime-10*86400, 10) + "')")
	if err != nil {
		t.Errorf(`Unexpected error: "%v"`, err)
	}

	// Sample data struct
	var (
		SampleData = []struct {
			days      int
			activity1 entity.Activity
			activity2 entity.Activity
		}{
			{
				days:      3,
				activity1: entity.Activity{UserID: userID1, Posts: 1, Comments: 2},
				activity2: entity.Activity{UserID: userID2, Posts: 1, Comments: 1},
			},
			{
				days:      7,
				activity1: entity.Activity{UserID: userID1, Posts: 2, Comments: 2},
				activity2: entity.Activity{UserID: userID2, Posts: 1, Comments: 1},
			},
			{
				days:      15,
				activity1: entity.Activity{UserID: userID1, Posts: 2, Comments: 3},
				activity2: entity.Activity{UserID: userID2, Posts: 1, Comments: 2},
			},
			{
				days:      30,
				activity1: entity.Activity{UserID: userID1, Posts: 3, Comments: 3},
				activity2: entity.Activity{UserID: userID2, Posts: 1, Comments: 2},
			},
		}
	)

	// Test function
	for _, sd := range SampleData {
		activities, err := GetActivity(sd.days)
		if err != nil {
			t.Errorf(`"%s" => unexpected error: "%v"`, strconv.Itoa(sd.days), err)
		}
		activity1 := activities[0].(entity.Activity)
		if activity1 != sd.activity1 {
			t.Errorf(`"%s" => unexpected error: "%v"`, strconv.Itoa(sd.days), "First user activities do not match")
		}
		activity2 := activities[1].(entity.Activity)
		if activity2 != sd.activity2 {
			t.Errorf(`"%s" => unexpected error: "%v"`, strconv.Itoa(sd.days), "Second user activities do not match")
		}
	}

	// Delete test users
	rows, err := db.Prepare("DELETE FROM users WHERE id = ?")
	if err == nil {
		exec, err := rows.Exec(userID1)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	rows, err = db.Prepare("DELETE FROM users WHERE id = ?")
	if err == nil {
		exec, err := rows.Exec(userID2)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	// Delete test posts
	rows, err = db.Prepare("DELETE FROM posts WHERE user = ?")
	if err == nil {
		exec, err := rows.Exec(userID1)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	rows, err = db.Prepare("DELETE FROM posts WHERE user = ?")
	if err == nil {
		exec, err := rows.Exec(userID2)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	// Delete test comments
	rows, err = db.Prepare("DELETE FROM comments WHERE user = ?")
	if err == nil {
		exec, err := rows.Exec(userID1)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
	rows, err = db.Prepare("DELETE FROM comments WHERE user = ?")
	if err == nil {
		exec, err := rows.Exec(userID2)
		if err == nil {
			_, err := exec.RowsAffected()
			if err != nil {
				t.Errorf(`Unexpected error: "%v"`, err)
			}
		} else {
			t.Errorf(`Unexpected error: "%v"`, err)
		}
	} else {
		t.Errorf(`Unexpected error: "%v"`, err)
	}
}
