package getactivity

import (
	entity "blog-motor/Entities/GetActivity"
	commentrepository "blog-motor/Repositories/Comments"
	postrepository "blog-motor/Repositories/Posts"
	userrepository "blog-motor/Repositories/Users"
)

// GetActivity - The get activity process
// @param {int} - Days
// @return {[]interface{}} - All activities
// @return {error}
func GetActivity(days int) ([]interface{}, error) {
	users, err := userrepository.GetAllUsers()
	if err == nil {
		var activities []interface{}
		for _, userID := range users {
			// Get posts number
			posts := postrepository.GetPostsNumByUser(userID, days)
			// Get comments number
			comments := commentrepository.GetCommentsNumByUser(userID, days)
			// Create new struct with activity data
			activity := entity.Activity{UserID: userID, Posts: posts, Comments: comments}
			// Add it to activities
			activities = append(activities, activity)
		}
		return activities, nil
	}
	return nil, err
}
